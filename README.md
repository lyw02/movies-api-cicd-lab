# Assignment 2 - Agile Software Practice.

Name: Yiwei Liu

## API endpoints.

+ GET /api/movies?page=:page - get paginated movie list
+ GET /api/movies/upcoming/list?page=:page - get paginated upcoming movie list
+ GET /api/movies/genres/list" - get all movie genres
+ GET /api/movies/:movieId/credits - get credits of a movie
+ GET /api/movies/actor/:actorId - get details of an specific actor
+ GET /api/favorites - get favorite movies list
+ POST /api/favorites/:movieId - add a movie to favorites
+ DELETE /api/favorites/:movieId - delete a movie from favorites

## Automated Testing.

The tests failed beacuse of a Mongoose/MongoDB error, but I have no idea about the reason. Therefore, I have to just record the actual result as follows:

~~~
PS D:\setu\Agile\Assignment 2\movies-api-cicd> npm run test

> movies-api@1.0.0 test
> cross-env NODE_ENV=test SEED_DB=true mocha

Warning: Cannot find any files matching pattern "test"
load user Data
load movies data
20
load genres data
load favorites data
Server running at 8080


  Movies endpoint
    GET /api/movies?page=1
      1) "before each" hook for "should return 20 movies from TMDB discover (page 1) and a status 200"
failed to Load user Data: MongooseError: Operation `users.deleteMany()` buffering timed out after 10000ms
failed to Load movie Data: MongooseError: Operation `movies.deleteMany()` buffering timed out after 10000ms
failed to Load genres Data: MongooseError: Operation `genres.deleteMany()` buffering timed out after 10000ms
failed to Load favorites Data: MongooseError: Operation `favorites.deleteMany()` buffering timed out after 10000ms       
failed to Load user Data: MongooseError: Operation `movies.insertMany()` buffering timed out after 10000ms
database connected to movies on ac-injf070-shard-00-01.cicyvon.mongodb.net

  Favorites endpoint
    GET /api/favorites
      2) should return all favorite movies of current user and a status 200
    POST /api/favorites/:movieId
      3) should add movie to favorites and return a status 200
    DELETE /api/favorites/:movieId
      when the movie id is valid
        4) should delete movie from favorites and return a status 200
    when the movie id is invalid
      5) should return the NOT found message


  0 passing (24s)
  5 failing

  1) Movies endpoint
       "before each" hook for "should return 20 movies from TMDB discover (page 1) and a status 200":
     Error: Timeout of 6000ms exceeded. For async tests and hooks, ensure "done()" is called; if returning a Promise, ensure it resolves. (D:\setu\Agile\Assignment 2\movies-api-cicd\tests\functional\api\movies\index.js)
      at listOnTimeout (node:internal/timers:564:17)
      at processTimers (node:internal/timers:507:7)

  2) Favorites endpoint
       GET /api/favorites
         should return all favorite movies of current user and a status 200:
     Uncaught AssertionError: expected {} to be an array
      at Test.<anonymous> (tests\functional\api\favorites\/index.js:51:34)
      at Test.assert (node_modules\supertest\lib\test.js:181:6)
      at Server.localAssert (node_modules\supertest\lib\test.js:131:12)
      at Object.onceWrapper (node:events:627:28)
      at Server.emit (node:events:513:28)
      at emitCloseNT (node:net:1880:8)
      at processTicksAndRejections (node:internal/process/task_queues:81:21)

  3) Favorites endpoint
       POST /api/favorites/:movieId
         should add movie to favorites and return a status 200:
     Error: Timeout of 6000ms exceeded. For async tests and hooks, ensure "done()" is called; if returning a Promise, ensure it resolves. (D:\setu\Agile\Assignment 2\movies-api-cicd\tests\functional\api\favorites\index.js)
      at listOnTimeout (node:internal/timers:564:17)
      at processTimers (node:internal/timers:507:7)

  4) Favorites endpoint
       DELETE /api/favorites/:movieId
         when the movie id is valid
           should delete movie from favorites and return a status 200:
     Error: Timeout of 6000ms exceeded. For async tests and hooks, ensure "done()" is called; if returning a Promise, ensure it resolves. (D:\setu\Agile\Assignment 2\movies-api-cicd\tests\functional\api\favorites\index.js)
      at listOnTimeout (node:internal/timers:564:17)
      at processTimers (node:internal/timers:507:7)

  5) Favorites endpoint
       when the movie id is invalid
         should return the NOT found message:

      Error: expected {
  status_code: 404,
  message: 'The resource you requested could not be found.'
} response body, got {}
      + expected - actual

      -{}
      +{
      +  "message": "The resource you requested could not be found."
      +  "status_code": 404
      +}

      at error (node_modules\supertest\lib\test.js:301:13)
      at Test._assertBody (node_modules\supertest\lib\test.js:205:14)
      at Test._assertFunction (node_modules\supertest\lib\test.js:283:11)
      at Test.assert (node_modules\supertest\lib\test.js:173:18)
      at Server.localAssert (node_modules\supertest\lib\test.js:131:12)
      at Object.onceWrapper (node:events:627:28)
      at Server.emit (node:events:513:28)
      at emitCloseNT (node:net:1880:8)
      at processTicksAndRejections (node:internal/process/task_queues:81:21)
~~~

## Deployments.

These deployments failed due to the same reason (Mongoose/MongoDB error when testing).

Staging: 

https://movies-api-staging-lyw-1a70b0363abd.herokuapp.com/

Production:

https://movies-api-production-lyw-37c17533d07e.herokuapp.com/

Dashboard:

https://dashboard.heroku.com/apps/movies-api-staging-lyw

GitLab:

https://gitlab.com/lyw02/movies-api-cicd-lab


## Independent Learning (if relevant)

Coverage report with Istanbul: (also see ./coverage/)

File                            | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
--------------------------------|---------|----------|---------|---------|-------------------------
All files                       |   57.85 |    30.92 |   50.51 |    68.2 |
 movies-api-cicd                |     100 |      100 |     100 |     100 |
  index.js                      |     100 |      100 |     100 |     100 |
 movies-api-cicd/api            |   31.81 |    24.24 |    37.5 |   35.71 |
  tmdb-api.js                   |   31.81 |    24.24 |    37.5 |   35.71 | 5-13,20-30
 movies-api-cicd/api/favorites  |    62.5 |    29.62 |      50 |   77.77 |
  favoriteModel.js              |    62.5 |    29.62 |      50 |   77.77 | 15-17
 movies-api-cicd/api/genres     |     100 |      100 |     100 |     100 |
  genreModel.js                 |     100 |      100 |     100 |     100 |
 movies-api-cicd/api/movies     |   40.98 |    21.73 |      35 |   56.09 |
  index.js                      |   35.18 |    18.18 |   33.33 |   51.42 | 12-31,39-48,55-56,64-65
  movieModel.js                 |   85.71 |      100 |      50 |   83.33 | 35
 movies-api-cicd/api/users      |    41.5 |    23.23 |   38.23 |   53.12 |
  index.js                      |   22.22 |     12.9 |   22.72 |    31.7 | 10-11,19-32,39-50,53-73
  userModel.js                  |   82.35 |    40.54 |   66.66 |    91.3 | 12,16
 movies-api-cicd/authenticate   |   21.87 |    21.62 |      50 |      25 |
  index.js                      |   21.87 |    21.62 |      50 |      25 | 6-23
 movies-api-cicd/db             |   83.33 |      100 |      50 |   81.81 |
  index.js                      |   83.33 |      100 |      50 |   81.81 | 11,14
 movies-api-cicd/errHandler     |      40 |        0 |       0 |      40 |
  index.js                      |      40 |        0 |       0 |      40 | 4-7
 movies-api-cicd/initialise-dev |     100 |      100 |     100 |     100 |
  movies.js                     |     100 |      100 |     100 |     100 |
  users.js                      |     100 |      100 |     100 |     100 |
 movies-api-cicd/seedData       |   94.93 |     57.4 |     100 |     100 |
  actor.js                      |     100 |      100 |     100 |     100 |
  favorites.js                  |     100 |      100 |     100 |     100 |
  genres.js                     |     100 |      100 |     100 |     100 |
  index.js                      |   94.28 |     57.4 |     100 |     100 | 2-61
  movies.js                     |     100 |      100 |     100 |     100 |
  users.js                      |     100 |      100 |     100 |     100 |

The configs of Coveralls are included (in GitLab CI/CD variables, .gitlab-ci.yml).

Coveralls webpage (the page cannot display report because the tests failed, as mentioned above):

https://coveralls.io/gitlab/lyw02/movies-api-cicd-lab


References:

+ https://istanbul.js.org/

+ https://docs.coveralls.io/