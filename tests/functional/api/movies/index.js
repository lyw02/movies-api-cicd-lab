import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";
import genres from "../../../../seedData/genres";
import actor from "../../../../seedData/actor";

const expect = chai.expect;
let db;

describe("Movies endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      // await Movie.deleteMany();
      await Movie.collection.insertMany(movies);
    } catch (err) {
      console.error(`failed to Load user Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });
  describe("GET /api/movies?page=1", () => {
    it("should return 20 movies from TMDB discover (page 1) and a status 200", (done) => {
      request(api)
        .get("/api/movies?page=1")
        .set("Accept", "application/json")
        // .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/movies/upcoming/list?page=1", () => {
    it("should return upcoming movies (page 1) and a status 200", (done) => {
      request(api)
        .get("/api/movies/?page=1")  // use same data with discover
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(20);
          done();
        });
    });
  });

  describe("GET /api/movies/genres/list", () => {
    it("should return all genres and a status 200", (done) => {
      request(api)
        .get("/api/movies/genres/list")
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(genres.length);
          done();
        });
    });
  });

  describe("GET /api/movies/:movieId/credits", () => {
    describe("when the movie id is valid", () => {
      it("should return the credits of the movie", () => {
        return request(api)
          .get(`/api/movies/${movies[0].id}/credits`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("name", actor[0].name);
          });
      });
    });
    describe("when the movie id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/9999/credits")
          .set("Accept", "application/json")
          .expect(404)
          .expect({
            status_code: 404,
            message: "The resource you requested could not be found.",
          });
      });
    });
  });

  describe("GET /api/movies/actor/:actorId", () => {
    describe("when the actor id is valid", () => {
      it("should return the actor data", () => {
        return request(api)
          .get(`/api/movies/actor/${actor[0].id}`)
          .set("Accept", "application/json")
          .expect(200)
          .then((res) => {
            expect(res.body).to.have.property("name", actor[0].name);
          });
      });
    });
    describe("when the actor id is invalid", () => {
      it("should return the NOT found message", () => {
        return request(api)
          .get("/api/movies/actor/9999")
          .set("Accept", "application/json")
          .expect(404)
          .expect({
            status_code: 404,
            message: "The actor you requested could not be found.",
          });
      });
    });
  });
  
});
