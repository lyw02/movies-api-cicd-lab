import chai from "chai";
import request from "supertest";
const mongoose = require("mongoose");
import Movie from "../../../../api/movies/movieModel";
import api from "../../../../index";
import movies from "../../../../seedData/movies";
import genres from "../../../../seedData/genres";
import actor from "../../../../seedData/actor";
import users from "../../../../seedData/users";
import Favorite from "../../../../api/favorites/favoriteModel";
import favorites from "../../../../seedData/favorites";

const expect = chai.expect;
let db;

describe("Favorites endpoint", () => {
  before(() => {
    mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    db = mongoose.connection;
  });

  after(async () => {
    try {
      await db.dropDatabase();
    } catch (error) {
      console.log(error);
    }
  });

  beforeEach(async () => {
    try {
      await Favorite.deleteMany();
      await Favorite.collection.insertMany(favorites);
    } catch (err) {
      console.error(`failed to Load favorite Data: ${err}`);
    }
  });
  afterEach(() => {
    api.close(); // Release PORT 8080
  });
  describe("GET /api/favorites", () => {
    it("should return all favorite movies of current user and a status 200", (done) => {
      request(api)
        .get("/api/favorites")
        .set("Accept", "application/json")
        .expect(200)
        .end((err, res) => {
          expect(res.body).to.be.a("array");
          expect(res.body.length).to.equal(favorites.length);
          done();
        });
    });
  });

  describe("POST /api/favorites/:movieId", () => {
    it("should add movie to favorites and return a status 200", (done) => {
      request(api)
        .post(`/api/favorites/${movies[1].id}`)
        .send({ user_id: users[0].id, movie_id: movies[1].id })
        .set("Accept", "application/json")
        .expect(200);
    });
  });

  describe("DELETE /api/favorites/:movieId", () => {
    describe("when the movie id is valid", () => {
      it("should delete movie from favorites and return a status 200", (done) => {
        request(api)
          .delete(`/api/favorites/${movies[1].id}`)
          .set("Accept", "application/json")
          .expect(200);
      });
    });
  });
  describe("when the movie id is invalid", () => {
    it("should return the NOT found message", () => {
      return request(api)
      .delete(`/api/favorites/9999999`)
        .set("Accept", "application/json")
        .expect(404)
        .expect({
          status_code: 404,
          message: "The resource you requested could not be found.",
        });
    });
  });
});
