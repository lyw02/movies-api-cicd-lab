import userModel from '../api/users/userModel';
import users from '../initialise-dev/users';
import dotenv from 'dotenv';
import genreModel from '../api/genres/genreModel'
import genres from './genres';
import movieModel from '../api/movies/movieModel';
import movies from '../initialise-dev/movies';
import favoriteModel from '../api/favorites/favoriteModel';
import favorites from '../seedData/favorites';

dotenv.config();

// deletes all user documents in collection and inserts test data
// deletes all user documents in collection and inserts test data
async function loadUsers() {
  console.log('load user Data');
  try {
    await userModel.deleteMany();
    await users.forEach(user => userModel.create(user));
    console.info(`${users.length} users were successfully stored.`);
  } catch (err) {
    console.error(`failed to Load user Data: ${err}`);
  }
}

// deletes all movies documents in collection and inserts test data
export async function loadMovies() {
  console.log('load movies data');
  console.log(movies.length);
  try {
    await movieModel.deleteMany();
    await movieModel.collection.insertMany(movies);
    console.info(`${movies.length} Movies were successfully stored.`);
  } catch (err) {
    console.error(`failed to Load movie Data: ${err}`);
  }
}

export async function loadGenres() {
  console.log('load genres data');
  try {
    await genreModel.deleteMany();
    await genreModel.collection.insertMany(genres);
    console.info(`${genres.length} Genres were successfully stored.`);
  } catch (err) {
    console.error(`failed to Load genres Data: ${err}`);
  }
}

export async function loadFavorites() {
  console.log('load favorites data');
  try {
    await favoriteModel.deleteMany();
    await favoriteModel.collection.insertMany(favorites);
    console.info(`${favorites.length} Favorite movies were successfully stored.`);
  } catch (err) {
    console.error(`failed to Load favorites Data: ${err}`);
  }
}

if (process.env.NODE_ENV === 'develop' || process.env.NODE_ENV === 'test') {
  loadUsers();
  loadMovies();
  loadGenres();
  loadFavorites();
}
